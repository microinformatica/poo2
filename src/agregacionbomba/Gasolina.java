/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacionbomba;

/**
 *
 * @author patri
 */
public  class Gasolina {
    
    private int id;
    private  int tipo;
    private String marca;
    private float precio;

    public Gasolina() {
       
        this.id = 0;
        this.tipo = 0;
        this.marca = "";
        this.precio = 0.0f;
    }

    public Gasolina(int id, int tipo, String marca, float precio) {
        this.id = id;
        this.tipo = tipo;
        this.marca = marca;
        this.precio = precio;
    }
    
    public Gasolina(Gasolina gasolina){
    
    this.id =gasolina.id;
    this.tipo = gasolina.tipo;
    this.marca = gasolina.marca;
    this.precio = gasolina.precio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
   public String mostarInformacion(){
       
       String informacion = "";
       
       informacion = "ID = " + 
               this.id + " Tipo " + 
               this.tipo + " Marca " +
               this.marca + " Precio" +
               this.precio;
       return informacion;
       
   } 
}  


