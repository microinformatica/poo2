/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agregacionbomba;

/**
 *
 * @author patri
 */
public class Bomba {
    private int numBomba;
    private float capacidad;
    private float contador;
    private Gasolina gasolina;
    
    
    //
    

    public Bomba() {
        
        this.numBomba = 0;
        this.capacidad = 0;
        this.contador = 0;
        this.gasolina = new Gasolina();
    }

    public Bomba(int numBomba, float capacidad, float contador, Gasolina gasolina) {
        this.numBomba = numBomba;
        this.capacidad = capacidad;
        this.contador = contador;
        this.gasolina = gasolina;
    }
    
    public Bomba(Bomba bomba){
        this.numBomba = bomba.numBomba;
        this.capacidad = bomba.capacidad;
        this.contador = bomba.contador;
        this.gasolina = bomba.gasolina;
    
    }

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public float getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(float capacidad) {
        this.capacidad = capacidad;
    }

    public float getContador() {
        return contador;
    }

    public void setContador(float contador) {
        this.contador = contador;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }
    
    public float obtenerInventaria(){
    return this.capacidad - this.contador;
        
        
  
    }
    public boolean realizarVenta(float cantidad){
    boolean exito = false;
    
    if( cantidad <= this.obtenerInventaria())exito=true;
    return exito;
    }
    
    
}
